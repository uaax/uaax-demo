/*
 * Uaax lazy-loading wrapper is a preferable solution accroding to
 * [latest comments](https://github.com/angular/angular-cli/issues/6373)
 * over [AOT approach](https://github.com/angular/angular-cli/issues/13386)
 */
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UaaxAccountModule, HttpLoaderFactory } from 'uaax';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateService } from '@ngx-translate/core';


@NgModule({
  imports: [
    // Below is optional for cases when TranslateModule.forRoot() is not initialized in the AppModule
    //
    // TranslateModule.forChild({
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: HttpLoaderFactory,
    //     deps: [ HttpClient ],
    //   },
    //   isolate: false,
    // }),
    UaaxAccountModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full' }
    ])
  ]
})
export class UaaxAccountLazyWrapperModule {}