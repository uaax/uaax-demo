import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }    from '@angular/router';

@Component({
  selector: 'uaax-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private pageTitle:string = 'Uaax demo';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.pageTitle = this.route.snapshot.data['title'];
  }

}
