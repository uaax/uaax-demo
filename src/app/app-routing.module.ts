import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {
      path: 'account',
      loadChildren: './uaa/account.module#UaaxAccountLazyWrapperModule'
  },
  {
    path: 'home',
    component: HomeComponent,
    data: {title: "Uaax Demo :: Home"},
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {title: "Uaax Demo :: Dashboard"},
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})
export class AppRoutingModule {}