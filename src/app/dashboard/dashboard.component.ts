import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }    from '@angular/router';

@Component({
  selector: 'uaax-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private pageTitle:string = 'Uaax demo';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.pageTitle = this.route.snapshot.data['title'];
  }
}
