import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { HttpClientModule } from '@angular/common/http';
// import { TranslateModule } from '@ngx-translate/core';
import { UaaxModule } from 'uaax';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,

    // Below is more an example than "defaults". Check the result of the call for yourself
    // and replace if required.
    ...UaaxModule.forRootImportDefaults(),
    // ^^ The above also imports UaaxModule.forRoot(config),

    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
