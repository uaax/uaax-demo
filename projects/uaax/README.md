# Uaax

Universal angular library for customizable user authentication and authorization clients. The project is hosted on [gitlab](https://gitlab.com/uaax/uaax-demo). The *uaax* is an angular 7+ library, it is placed inside the demo example code which includes how to import and hook it to your application. 

To learn more on how to integrate the lib into your app and use it in your project, look up the routing code for the lazy module-loading: 

1. [app/uaa/account.module.ts](https://gitlab.com/uaax/uaax-demo/blob/master/src/app/uaa/account.module.ts)
2. [app/app-routing.module.ts](https://gitlab.com/uaax/uaax-demo/blob/master/src/app/app-routing.module.ts)

## Status

Currently it is based mostly on pieces of JHipster client code, packaged as a separated middle-ware or angular lib. The idea is to be able to rather effortlessly link user registration and management workflow to any new app (which works with the JHipster generated API on the server side).

The code is in beta state and is very much work in progress.
