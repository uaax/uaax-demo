import { Injectable } from '@angular/core';
import { faSort, faSortDown, faSortUp, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Injectable({
  providedIn: 'root'
})
export class UaaxConfig {

  serverApiUrl? = '/uaax/';

  routeParentUrl? = '/account';

  navigateHomeByDefault?:boolean = true;

  // TODO: implement internal error handling inside the '/account/errors/*'
  doErrorHandling?:boolean = false;

}