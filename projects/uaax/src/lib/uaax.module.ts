/**
 * You can statically import this module from you root-level (AppModule)
 * in order to quickly load and confugre basic pre-requisites like ngx-translate.
 *
 * TODO: implement forChild() to support lazy loading? This we have to re-do forRoot()
 * calls which are anyway a questionable shortcut here
 */
import { NgModule, ModuleWithProviders } from '@angular/core';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from './utils/language/http-loader-factory';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxWebstorageModule } from 'ngx-webstorage';
// import { NgJhipsterModule } from 'ng-jhipster';
import { UaaxSharedModule } from './shared';
import { UaaxConfig } from './uaax-config';
import { UaaxConfigService } from './uaax-config.service';


@NgModule({
  imports: [UaaxSharedModule],
  exports: [UaaxSharedModule],
})
export class UaaxModule {

  /**
   * Ideally this method should be considered as just an example that can be imported
   * by your AppModule, i.e.
   *
   * imports: [
   *   ...UaaxModule.forRootImportDefaults(),
   * ]
   *
   */
  static forRootImportDefaults(config: UaaxConfig = {}): any[] {
    return [
      HttpClientModule,
      // TODO remove this once it is fully replaced
      // NgJhipsterModule.forRoot({
      //   // set below to true to make alerts look like toast
      //   alertAsToast: false,
      //   alertTimeout: 5000,
      //   i18nEnabled: true,
      //   defaultI18nLang: 'en'
      // }),
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [ HttpClient ],
        },
      }),
      NgbModule.forRoot(),
      NgxWebstorageModule.forRoot({ prefix: 'jhi', separator: '-' }),
      UaaxModule.forRoot(config),
    ]
  }

  /**
   * Although almost all services are injected with annotation {providedIn: 'root'},
   * this is still usefull to pass the config.
   */
  static forRoot(config: UaaxConfig = {}): ModuleWithProviders {
    return {
      ngModule: UaaxModule,
      providers: [
        { provide: UaaxConfig, useValue: config },
        {
          provide: UaaxConfigService,
          useClass: UaaxConfigService,
          deps: [UaaxConfig]
        },
      ],
    };
  }

}
