import { HttpClient } from '@angular/common/http';
import { UaaxConfig } from './uaax-config';
import { UaaxConfigService } from './uaax-config.service';


export { UaaxConfig, UaaxConfigService };

export class UaaxBaseServerApiService {

  constructor(protected uaax: UaaxConfigService, protected http: HttpClient) {}

  public get config(): UaaxConfig {
    return this.uaax.getConfig();
  }

  public getServerApiUrl(): string {
    return this.uaax.getConfig().serverApiUrl;
  }

}

