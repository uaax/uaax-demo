/*
 * Define sub-routes for <root_url>/.../{login,logout,..} etc to be customly attached
 * and nested.

 * Usage example (assuming 'uaax' is in 'node_modules/' or otherwise configured available
 * e.g. with 'paths' in tsconfig.json):
 *
 *   const routes: Routes = [
 *     {
 *       path: 'account',
 *       loadChildren: 'uaax/account/account.module#UaaxAccountModule'
 *     },
 *     ..
 *   ]
 *
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UaaxUserRouteAccessService } from '../auth/user-route-access.service';
import { UaaxLoginComponent } from './login/login.component';
import { UaaxLogoutComponent } from './logout/logout.component';
import { UaaxActivateComponent } from './activate/activate.component';
import { UaaxPasswordResetInitComponent } from './password-reset/init/password-reset-init.component';
import { UaaxPasswordResetFinishComponent } from './password-reset/finish/password-reset-finish.component';
import { UaaxRegisterComponent } from './register/register.component';
import { UaaxPasswordComponent } from './password/password.component';
import { UaaxSettingsComponent } from './settings/settings.component';

const routes: Routes = [{
  // Path is set externally at the lazy loading of the <account> module.
  path: '',
  children: [
    {
      path: 'login',
      component: UaaxLoginComponent,
    },
    {
      path: 'logout',
      component: UaaxLogoutComponent,
    },
    {
      path: 'activate',
      component: UaaxActivateComponent,
      data: {
        authorities: [],
      }
    },
    {
      path: 'reset/request',
      component: UaaxPasswordResetInitComponent,
      data: {
          authorities: [],
      }
    },
    {
      path: 'reset/finish',
      component: UaaxPasswordResetFinishComponent,
      data: {
          authorities: [],
      }
    },
    {
      path: 'register',
      component: UaaxRegisterComponent,
      data: {
          authorities: [],
      }
    },
    {
      path: 'password',
      component: UaaxPasswordComponent,
      data: {
        authorities: ['ROLE_USER'],
      },
      canActivate: [UaaxUserRouteAccessService]
    },
    {
      path: 'settings',
      component: UaaxSettingsComponent,
      data: {
          authorities: ['ROLE_USER'],
      },
      canActivate: [UaaxUserRouteAccessService]
    },
  ],
}];


@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ],
})
export class UaaxAccountRoutesModule { }
