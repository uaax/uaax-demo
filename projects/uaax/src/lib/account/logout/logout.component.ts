import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UaaxLoginService } from '../../auth/login.service';


@Component({
  selector: 'uaax-logout',
  template: '',
})
export class UaaxLogoutComponent implements OnInit {

    constructor(
        private loginService: UaaxLoginService,
        private router: Router,
    ) {}

    ngOnInit(): void {
      this.logout();
    }

    logout() {
        this.loginService.logout();
        this.router.navigate(['']);
    }
}
