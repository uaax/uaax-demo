import { NgModule } from '@angular/core';
import { UaaxSharedModule } from '../shared';
import { UaaxAccountRoutesModule } from './account-routes.module';
import { UaaxLoginComponent } from './login/login.component';
import { UaaxLogoutComponent } from './logout/logout.component';
import { UaaxPasswordStrengthBarComponent } from './password/password-strength-bar.component';
import { UaaxPasswordComponent } from './password/password.component';
import { UaaxActivateComponent } from './activate/activate.component';
import { UaaxPasswordResetInitComponent } from './password-reset/init/password-reset-init.component';
import { UaaxPasswordResetFinishComponent } from './password-reset/finish/password-reset-finish.component';
import { UaaxRegisterComponent } from './register/register.component';
import { UaaxSettingsComponent } from './settings/settings.component';


@NgModule({
  imports: [
    // CommonModule, FormsModule.. are picked up from shared module
    UaaxSharedModule,
    UaaxAccountRoutesModule,
  ],
  declarations: [
    UaaxLoginComponent,
    UaaxLogoutComponent,
    UaaxPasswordStrengthBarComponent,
    UaaxPasswordComponent,
    UaaxPasswordResetInitComponent,
    UaaxPasswordResetFinishComponent,
    UaaxActivateComponent,
    UaaxRegisterComponent,
    UaaxSettingsComponent,
  ]
})
export class UaaxAccountModule { }
