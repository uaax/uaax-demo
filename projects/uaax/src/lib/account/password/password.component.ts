import { Component, OnInit } from '@angular/core';

import { UaaxAccountService } from '../../auth/account.service';
import { UaaxPasswordService } from './password.service';

@Component({
    selector: 'uaax-password',
    templateUrl: './password.component.html',
    styleUrls: ['../account.scss'],
})
export class UaaxPasswordComponent implements OnInit {
    doNotMatch: string;
    error: string;
    success: string;
    account: any;
    currentPassword: string;
    newPassword: string;
    confirmPassword: string;

    constructor(private passwordService: UaaxPasswordService, private accountService: UaaxAccountService) {}

    ngOnInit() {
        this.accountService.identity().then(account => {
            this.account = account;
        });
    }

    changePassword() {
        if (this.newPassword !== this.confirmPassword) {
            this.error = null;
            this.success = null;
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.passwordService.save(this.newPassword, this.currentPassword).subscribe(
                () => {
                    this.error = null;
                    this.success = 'OK';
                },
                () => {
                    this.success = null;
                    this.error = 'ERROR';
                }
            );
        }
    }
}
