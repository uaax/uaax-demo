import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {
  UaaxConfig,
  UaaxConfigService,
  UaaxBaseServerApiService } from '../../base-server-api.service';


@Injectable({ providedIn: 'root' })
export class UaaxPasswordService extends UaaxBaseServerApiService {

    save(newPassword: string, currentPassword: string): Observable<any> {
        return this.http.post(this.getServerApiUrl() + 'uaa/api/account/change-password',
          { currentPassword, newPassword });
    }

}
