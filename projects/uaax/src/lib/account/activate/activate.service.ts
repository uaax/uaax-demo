import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  UaaxConfig,
  UaaxConfigService,
  UaaxBaseServerApiService } from '../../base-server-api.service';


@Injectable({ providedIn: 'root' })
export class UaaxActivateService extends UaaxBaseServerApiService {

  get(key: string): Observable<any> {
    return this.http.get(this.getServerApiUrl() + 'uaa/api/activate', {
      params: new HttpParams().set('key', key)
    });
  }

}
