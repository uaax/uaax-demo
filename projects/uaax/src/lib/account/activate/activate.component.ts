import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UaaxNavigationService } from '../../auth/navigation.service';
import { UaaxActivateService } from './activate.service';

@Component({
    selector: 'uaax-activate',
    templateUrl: './activate.component.html',
    styleUrls: ['../account.scss'],
})
export class UaaxActivateComponent implements OnInit {
    error: string;
    success: string;

    constructor(private activateService: UaaxActivateService, private navigation: UaaxNavigationService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.activateService.get(params['key']).subscribe(
                () => {
                    this.error = null;
                    this.success = 'OK';
                },
                () => {
                    this.success = null;
                    this.error = 'ERROR';
                }
            );
        });
    }

    login() {
        this.navigation.navigateTo('/login');
    }
}
