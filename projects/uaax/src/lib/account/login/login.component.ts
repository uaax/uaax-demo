import { Component, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
// TODO: replace JhiEventManager and ng-jhipster dependency
import { JhiEventManager } from 'ng-jhipster';

import { UaaxLoginService } from '../../auth/login.service';
import { UaaxStateStorageService } from '../../auth/state-storage.service';
import { UaaxNavigationService } from '../../auth/navigation.service';


@Component({
    selector: 'uaax-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class UaaxLoginComponent implements AfterViewInit {
    authenticationError: boolean;
    password: string;
    rememberMe: boolean;
    username: string;
    credentials: any;

    constructor(
        private eventManager: JhiEventManager,
        private loginService: UaaxLoginService,
        private stateStorageService: UaaxStateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        private navigation: UaaxNavigationService,
    ) {
        this.credentials = {};
    }

    ngAfterViewInit() {
        setTimeout(() => this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []), 0);
    }

    cancel() {
        this.credentials = {
            username: null,
            password: null,
            rememberMe: true
        };
        this.authenticationError = false;
    }

    login() {
        this.loginService
            .login({
                username: this.username,
                password: this.password,
                rememberMe: this.rememberMe
            })
            .then(() => {
                this.authenticationError = false;
                let activateRe = new RegExp('^' + this.navigation.getParentUrl() + '/activate/');
                let resetRe = new RegExp('^' + this.navigation.getParentUrl() + '/reset/');
                if (this.router.url === this.navigation.getParentUrl() + '/register' || activateRe.test(this.router.url) || resetRe.test(this.router.url)) {
                    this.router.navigate(['']);
                }

                this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });

                // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // since login is successful, go to stored previousState and clear previousState
                const redirect = this.stateStorageService.getUrl();
                if (redirect) {
                    this.stateStorageService.storeUrl(null);
                    this.router.navigate([redirect]);
                } else {
                    this.navigation.navigateHome();
                }
            })
            .catch(() => {
                this.authenticationError = true;
            });
    }

    register() {
        this.navigation.navigateTo('/register');
    }

    requestResetPassword() {
        this.navigation.navigateTo('/reset/request');
    }
}
