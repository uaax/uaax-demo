import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UaaxLoginComponent } from './login.component';

describe('UaaxLoginComponent', () => {
  let component: UaaxLoginComponent;
  let fixture: ComponentFixture<UaaxLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UaaxLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UaaxLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
