import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  UaaxConfig,
  UaaxConfigService,
  UaaxBaseServerApiService } from '../../../base-server-api.service';


@Injectable({ providedIn: 'root' })
export class UaaxPasswordResetFinishService extends UaaxBaseServerApiService {

    save(keyAndPassword: any): Observable<any> {
        return this.http.post(this.getServerApiUrl() + 'uaa/api/account/reset-password/finish', keyAndPassword);
    }

}
