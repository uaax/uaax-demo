import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UaaxNavigationService } from '../../../auth/navigation.service';
import { UaaxPasswordResetFinishService } from './password-reset-finish.service';

@Component({
    selector: 'uaax-password-reset-finish',
    templateUrl: './password-reset-finish.component.html',
    styleUrls: ['../../account.scss'],
})
export class UaaxPasswordResetFinishComponent implements OnInit, AfterViewInit {
    confirmPassword: string;
    doNotMatch: string;
    error: string;
    keyMissing: boolean;
    resetAccount: any;
    success: string;
    key: string;

    constructor(
        private passwordResetFinishService: UaaxPasswordResetFinishService,
        private navigation: UaaxNavigationService,
        private route: ActivatedRoute,
        private elementRef: ElementRef,
        private renderer: Renderer
    ) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.key = params['key'];
        });
        this.resetAccount = {};
        this.keyMissing = !this.key;
    }

    ngAfterViewInit() {
        if (this.elementRef.nativeElement.querySelector('#password') != null) {
            this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#password'), 'focus', []);
        }
    }

    finishReset() {
        this.doNotMatch = null;
        this.error = null;
        if (this.resetAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.passwordResetFinishService.save({ key: this.key, newPassword: this.resetAccount.password }).subscribe(
                () => {
                    this.success = 'OK';
                },
                () => {
                    this.success = null;
                    this.error = 'ERROR';
                }
            );
        }
    }

    login() {
        this.navigation.navigateTo('/login');
    }
}
