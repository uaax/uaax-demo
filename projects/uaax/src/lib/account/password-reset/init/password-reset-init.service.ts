import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  UaaxConfig,
  UaaxConfigService,
  UaaxBaseServerApiService } from '../../../base-server-api.service';


@Injectable({ providedIn: 'root' })
export class UaaxPasswordResetInitService extends UaaxBaseServerApiService {

    save(mail: string): Observable<any> {
        return this.http.post(this.getServerApiUrl() + 'uaa/api/account/reset-password/init', mail);
    }

}
