import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

// import { NgbDateMomentAdapter } from './util/datepicker-adapter';
// import { UaaxSharedLibsModule, UaaxSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { UaaxSharedLibsModule } from './shared-libs.module';
import { UaaxSharedCommonModule } from './shared-common.module';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';

@NgModule({
    imports: [UaaxSharedLibsModule, UaaxSharedCommonModule],
    declarations: [HasAnyAuthorityDirective],
    // providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    // entryComponents: [JhiLoginModalComponent],
    exports: [UaaxSharedCommonModule, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UaaxSharedModule {
    static forRoot() {
        return {
            ngModule: UaaxSharedModule
        };
    }
}
