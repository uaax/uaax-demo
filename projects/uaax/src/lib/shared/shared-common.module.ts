import { NgModule } from '@angular/core';
import { JhiAlertComponent } from './alert/alert.component';
import { JhiAlertErrorComponent } from './alert/alert-error.component';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { UaaxSharedLibsModule } from './shared-libs.module';



@NgModule({
    imports: [UaaxSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent, FindLanguageFromKeyPipe],
    exports: [UaaxSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent, FindLanguageFromKeyPipe]
})
export class UaaxSharedCommonModule {}
