import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { UaaxConfigService } from '../uaax-config.service';

@Injectable({ providedIn: 'root' })
export class UaaxNavigationService {

  constructor(private router: Router, private uaax: UaaxConfigService) {}

  getParentUrl() {
      return this.uaax.getConfig().routeParentUrl;
  }

  navigateTo(subpath: string) {
    this.router.navigate([this.getParentUrl() + subpath]);
  }

  /**
   * If navigateHomeByDefault is true, will navigate to top route which is usually \
   * pointing to home page.
   */
  navigateHome() {
    if (this.uaax.getConfig().navigateHomeByDefault) {
      // navigate home as default behavior
      this.router.navigate(['']);
    }
  }

  handleAccessDenied(notAuthorized: boolean) {
    if (notAuthorized) {
      // only redirect and ask to login, if the user hasn't done that yet
      this.navigateTo('/login');
    } else if (this.uaax.getConfig().doErrorHandling) {
      this.navigateTo('/errors/accessdenied');
    }
  }
}
