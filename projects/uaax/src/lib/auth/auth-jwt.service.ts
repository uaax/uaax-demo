import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  UaaxConfig,
  UaaxConfigService,
  UaaxBaseServerApiService } from '../base-server-api.service';


@Injectable({
    providedIn: 'root',
    deps: [UaaxConfigService],
})
export class UaaxAuthServerProvider extends UaaxBaseServerApiService {

    getToken() {
        return null;
    }

    login(credentials): Observable<any> {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        return this.http.post(this.getServerApiUrl() + 'auth/login', data, {});
    }

    loginWithToken(jwt, rememberMe) {
        if (jwt) {
            this.storeAuthenticationToken(jwt, rememberMe);
            return Promise.resolve(jwt);
        } else {
            return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
        }
    }

    storeAuthenticationToken(jwt, rememberMe) {}

    logout(): Observable<any> {
        return this.http.post(this.getServerApiUrl() + 'auth/logout', null);
    }
}
