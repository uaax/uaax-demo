import { Injectable } from '@angular/core';

import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';
import { IUser } from '../data/user.model';
import { UaaxAccountService } from './account.service';
import { UaaxAuthServerProvider } from './auth-jwt.service';
// import { JhiTrackerService } from '../tracker/tracker.service';


@Injectable({ providedIn: 'root' })
export class UaaxLoginService {
    constructor(
        private accountService: UaaxAccountService,
        // private trackerService: JhiTrackerService,
        private languageService: JhiLanguageService,
        private sessionStorage: SessionStorageService,
        private authServerProvider: UaaxAuthServerProvider
    ) {}

    login(credentials, callback?) {
        const cb = callback || function() {};

        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe(
                data => {
                    this.accountService.identity(true).then(account => {
                        // this.trackerService.sendActivity();
                        this.setPreferredLanguage(account);
                        resolve(data);
                    });
                    return cb();
                },
                err => {
                    this.logout();
                    reject(err);
                    return cb(err);
                }
            );
        });
    }

    setPreferredLanguage(account: IUser) {
        const langKey = account.langKey || this.sessionStorage.retrieve('locale');
        this.languageService.changeLanguage(langKey);
    }

    logout() {
        if (this.accountService.isAuthenticated()) {
            this.authServerProvider.logout().subscribe(() => this.accountService.authenticate(null));
        } else {
            this.accountService.authenticate(null);
        }
    }
}
