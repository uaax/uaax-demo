import { Injectable } from '@angular/core';
import { UaaxConfig } from './uaax-config';

@Injectable({
    providedIn: 'root'
})
export class UaaxConfigService {

  configValues: UaaxConfig;

  constructor(config?: UaaxConfig) {
    this.configValues = {
      ...new UaaxConfig(),
      ...config
    };
  }

  getConfig(): UaaxConfig {
    return this.configValues;
  }
}