import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UaaxConfigService } from '../uaax-config.service';
import { createRequestOption } from '../utils/request-util';
import { IUser } from './user.model';


@Injectable({
    providedIn: 'root',
    deps: [UaaxConfigService],
})
export class UserService {

    constructor(private http: HttpClient, private uaax: UaaxConfigService) {}

    get resourceUrl(): string {
        return this.uaax.getConfig().serverApiUrl + 'uaa/api/users';
    }

    create(user: IUser): Observable<HttpResponse<IUser>> {
        return this.http.post<IUser>(this.resourceUrl, user, { observe: 'response' });
    }

    update(user: IUser): Observable<HttpResponse<IUser>> {
        return this.http.put<IUser>(this.resourceUrl, user, { observe: 'response' });
    }

    find(login: string): Observable<HttpResponse<IUser>> {
        return this.http.get<IUser>(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }

    query(req?: any): Observable<HttpResponse<IUser[]>> {
        const options = createRequestOption(req);
        return this.http.get<IUser[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(login: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }

    authorities(): Observable<string[]> {
        return this.http.get<string[]>(this.uaax.getConfig().serverApiUrl + 'uaa/api/users/authorities');
    }
}
