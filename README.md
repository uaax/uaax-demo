# UaaxDemo

The `uaax-demo` is an app hosting library code in `projects/uaax` which is compiled into `dist/uaax`. For details inspect `agular.json`

#### Installation

The following installation as an npm package should work:

	npm install uaax --save


#### Develop


##### Connect to API

If you run server-side uaax API e.g. jHispter generated and dockerized gateway on the standard http://localhost:8080 which exposes all the http://localhost:8080/auth/* and http://localhost:8080/uaa/api/account/* calls, then it maybe convenient to circumvent CORS in a local dev environment using `proxy.conf.json`. That means you should start serving your app with

	ng serve uaax-demo --proxy-config proxy.conf.json


##### Other angular CLI commands

Here goes notes copied from an angular vanilla project 

###### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

###### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

###### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

###### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

###### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

##### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
